#include "PixelMonitoring/PixelAthClusterMonAlg.h"
#include "PixelMonitoring/PixelAthErrorMonAlg.h"
#include "PixelMonitoring/PixelAthHitMonAlg.h"

DECLARE_COMPONENT( PixelAthClusterMonAlg )
DECLARE_COMPONENT( PixelAthErrorMonAlg )
DECLARE_COMPONENT( PixelAthHitMonAlg )
