/* Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
   Author: Andrii Verbytskyi andrii.verbytskyi@mpp.mpg.de
*/
#ifndef ATLASHEPMC_HEPEVTWRAPPER_H
#define ATLASHEPMC_HEPEVTWRAPPER_H
#include "HepMC/HEPEVT_Wrapper.h"
#endif
